## 1- Basic querying

```python
def say_hello(request):
    query_set = Product.objects.all()
    item_count = Product.objects.count()
    print(item_count)

        for product in query_set:
            print(product.title)
        return render(request, 'hello.html', {'name': item_count})
```

## 2 - Retreving objects

### With get()

```python
from django.core.exceptions import ObjectDoesNotExist

def say_hello(request):
    try:
        product = Product.objects.get(pk=1)
    except ObjectDoesNotExist:
        pass
    return render(request, 'hello.html', {'name': product.title})
```

### With filter() - Returns None if it doesn't exist

```python
def say_hello(request):
    product = Product.objects.filter(pk=0).first()
    product_exists = Product.objects.filter(pk=0).exists()
    return render(request, 'hello.html', {'name': product})
```

### Field lookups

```html
<html>
  <body>
    {% if name %}
    <h1>{{ name }}</h1>
    {% else %}
    <h1>Hello World</h1>
    {% endif %} {% for query in query_list%}
    <ul>
      {% for item in query %}
      <li>{{ item.title }}</li>
      {% endfor %}
    </ul>

    <hr />
    {% endfor %}
  </body>
</html>
```

```python
def say_hello(request):
    queryset = Product.objects.filter(unit_price__gt=20)
    queryset_range = Product.objects.filter(unit_price__range=(20, 30))
    queryset_contains = Product.objects.filter(title__contains='sauce') # For case insensitive use 'icontains' instead of 'contains'
    queryset_date = Product.objects.filter(last_update__year=2021)
    queryset_null = Product.objects.filter(title__isnull=True)
    query_list= [queryset,queryset_range,queryset_contains,queryset_date,queryset_null]
    return render(request, 'hello.html', {'name': 'Check DBGtool','query_list':query_list})

```

## Complex looks up with Q

```python
from django.db.models import Q

def say_hello(request):
    # queryset = Product.objects.filter(inventory__lt=10,unit_price__gt=20)
    # queryset = Product.objects.filter(inventory__lt=10).filter(unit_price__gt=20)
    # OR operator
    queryset = Product.objects.filter(Q(inventory__lt=10) | Q(unit_price__gt=20))
    # AND operator
    queryset2 = Product.objects.filter(Q(inventory__lt=10) & ~Q(unit_price__gt=20))
    query_list= [queryset,queryset2]
    return render(request, 'hello.html', {'name': 'Check DBGtool','query_list':query_list})
```

## Referencing fields with F objects

```python
from django.db.models import Q, F

def say_hello(request):
    # queryset = Product.objects.filter(inventory = unit_price)
    # F object
    # queryset = Product.objects.filter(inventory = F('unit_price'))
    queryset = Product.objects.filter(inventory = F('collection__id'))
    query_list = [queryset]
    return render(request, 'hello.html', {'name': 'Check DBGtool','query_list':query_list})
```

## Sorting

```python
def say_hello(request):
    # Ascending
    queryset = Product.objects.order_by('title')
    # Descending
    queryset = Product.objects.order_by('-title')
    # Multiple fields
    queryset = Product.objects.order_by('unit_price', '-title')
    # Reverse the sort
    queryset = Product.objects.order_by('unit_price', '-title').reverse()
    # Chaining filter and sort
    queryset = Product.objects.filter(unit_price__gt=20).order_by('unit_price')
    # accessing individual elements
    product = Product.objects.order_by('unit_price')[0]
    # Earliest
    product = Product.objects.earliest('unit_price')
    # latest
    product = Product.objects.latest('unit_price')
    query_list = [queryset]
    return render(request, 'hello.html', {'name': 'Check DBGtool','query_list':query_list})

```

## Limiting

```python
def say_hello(request):
    # Ascending
    queryset = Product.objects.order_by('title')[:10]
    query_list = [queryset]
    return render(request, 'hello.html', {'name': 'Check DBGtool','query_list':query_list})
```

## Selecting fields for Query

```python
def say_hello(request):
    queryset = Product.objects.values("title", "unit_price", "collection__title")
    queryset = Product.objects.values_list("title", "unit_price", "collection__title")
    # Accesing related objects
    queryset = OrderItem.objects.values(
        "product__id",
        "product__title",
        "product__unit_price",
        "product__collection__title",
    ).order_by("product__id").distinct()

    query_list = [queryset]
    return render(
        request, "hello.html", {"name": "Check DBGtool", "query_list": query_list}
    )
```

### If you have to filter using `values`

```python
def say_hello(request):
    queryset = Product.objects.values("title", "unit_price", "collection__title")
    queryset = Product.objects.values_list("title", "unit_price", "collection__title")
    # Accesing related objects
    queryset = OrderItem.objects.values(
        "product__id"
    ).order_by("product__id").distinct()
    queryset = Product.objects.filter(id__in=queryset)
    # queryset = Product.objects.filter(~Q(id__in=queryset))

    query_list = [queryset]
    return render(
        request, "hello.html", {"name": "Check DBGtool", "query_list": query_list}
    )

```

## Deferring fields

### Only

`Be very careful with only()`

```python
def say_hello(request):
    queryset = Product.objects.only("id","title")
    query_list = [queryset]
    return render(
        request, "hello.html", {"name": "Check DBGtool", "query_list": query_list}
    )
```

```html
<html>
  <body>
    {% if name %}
    <h1>{{ name }}</h1>
    {% else %}
    <h1>Hello World</h1>
    {% endif %} {% for query in query_list%}
    <ul>
      {% for item in query %}
      <!-- unit_price should be removed -->
      <li>{{item.title}},{{item.id}}{{item.unit_price}}</li>
      {% endfor %}
    </ul>

    <hr />
    {% endfor %}
    <!-- {{ query_list}} -->
  </body>
</html>
```

### Defer

```python
def say_hello(request):
    queryset = Product.objects.defer("description")
    query_list = [queryset]
    return render(
        request, "hello.html", {"name": "Check DBGtool", "query_list": query_list}
    )
```

## Selecting related objects

```html
<html>
  <body>
    {% if name %}
    <h1>{{ name }}</h1>
    {% else %}
    <h1>Hello World</h1>
    {% endif %} {% for query in query_list%}
    <ul>
      {% for item in query %}
      <!-- MAke sure to remove item.collection.title for prefetch -->
      <li>{{item.title}},{{item.collection.title}}</li>
      {% endfor %}
    </ul>

    <hr />
    {% endfor %}
    <!-- {{ query_list}} -->
  </body>
</html>
```

```python
def say_hello(request):
    queryset = Product.objects.all() # Only going to query products table in the database not other tables
    # Selected related (1) - This will query the collection table as well
    queryset = Product.objects.select_related("collection").all()
    query_list = [queryset]
    return render(
        request, "hello.html", {"name": "Check DBGtool", "query_list": query_list}
    )

```

## Prefetching related objects

```html
<html>
  <body>
    {% if name %}
    <h1>{{ name }}</h1>
    {% else %}
    <h1>Hello World</h1>
    {% endif %} {% for query in query_list%}
    <div>
      {% for product in query %}
      <div>
        <h2>Product Title: {{ product.title }}</h2>
        <p>Description: {{ product.description }}</p>
        <p>Unit Price: ${{ product.unit_price }}</p>
        <p>Inventory: {{ product.inventory }}</p>
        <p>Last Update: {{ product.last_update }}</p>

        <h3>Promotions:</h3>
        <ul>
          {% for promotion in product.promotions.all %}
          <li>
            Description: {{ promotion.description }} - Discount: {{
            promotion.discount }}%
          </li>
          {% empty %}
          <li>No promotions available for this product.</li>
          {% endfor %}
        </ul>
      </div>
      {% endfor %}
    </div>

    <hr />
    {% endfor %}
    <!-- {{ query_list}} -->
  </body>
</html>
```

```python
def say_hello(request):
    queryset = Product.objects.all() # Only going to query products table in the database not other tables
    # Prefetch related (n) - This will query the promotion table as well
    queryset = Product.objects.prefetch_related("promotions").all()
    query_list = [queryset]
    return render(
        request, "hello.html", {"name": "Check DBGtool", "query_list": query_list}
    )

```

## Combining selected related and prefetch related

```html
<html>
  <body>
    {% if name %}
    <h1>{{ name }}</h1>
    {% else %}
    <h1>Hello World</h1>
    {% endif %} {% for query in query_list%}
    <div>
      {% for product in query %}
      <div>
        <h2>Product Title: {{ product.title }}</h2>
        <p>Description: {{ product.description }}</p>
        <p>Unit Price: ${{ product.unit_price }}</p>
        <p>Inventory: {{ product.inventory }}</p>
        <p>Last Update: {{ product.last_update }}</p>
        <p>Category: {{ product.collection.id }}</p>

        <h3>Promotions:</h3>
        <ul>
          {% for promotion in product.promotions.all %}
          <li>
            Description: {{ promotion.description }} - Discount: {{
            promotion.discount }}%
          </li>
          {% empty %}
          <li>No promotions available for this product.</li>
          {% endfor %}
        </ul>
      </div>
      {% endfor %}
    </div>

    <hr />
    {% endfor %}
    <!-- {{ query_list}} -->
  </body>
</html>
```

```python
from django.shortcuts import render
from django.http import HttpResponse
from store.models import Product, OrderItem
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q, F


def say_hello(request):
    queryset = (
        Product.objects.prefetch_related("promotions")
        .select_related("collection")
        .all()
    )
    query_list = [queryset]
    return render(
        request, "hello.html", {"name": "Check DBGtool", "query_list": query_list}
    )

```

## Practice on related

```html
<html>

<body>
  {% if name %}
  <h1>{{ name }}</h1>
  {% else %}
  <h1>Hello World</h1>
  {% endif %}
  {% for query in query_list%}
  <div>
    {% for order in query %}
    <div>
      <p>{{ order.id }}-{{ order.customer.first_name }}</p>
      {% for item in order.orderitem_set.all %}
      <p>{{ item.product.title }}</p>
      {% endfor %}
      <br>
      {% endfor %}
    </div>

    <hr>
    {% endfor %}
    <!-- {{ query_list}} -->
</body>

</html>
```

```python

def say_hello(request):
    queryset = (
        Product.objects.all()
    )  # Only going to query products table in the database not other tables
    # Prefetch related (n) - This will query the promotion table as well
    queryset = (
        Order.objects.prefetch_related('orderitem_set__product').select_related('customer').order_by('-placed_at')[:5]
    )
    query_list = [queryset]
    return render(
        request, "hello.html", {"name": "Check DBGtool", "query_list": query_list}
    )

```

## Aggregating objects

```html
<html>
  <body>
    {% if name %}
    <h1>{{ name }}</h1>
    {% else %}
    <h1>Hello World</h1>
    {% endif %}
    <div>{{result}}</div>
    <br />
    <div>{{result_filtered}}</div>
  </body>
</html>
```

```python
from django.db.models import Count, Avg, Sum, Min, Max


def say_hello(request):
    result = Product.objects.aggregate(
        count=Count("id"),
        min_price=Min("unit_price"),
        max_price=Max("unit_price"),
        avg_price=Avg("unit_price"),
        sum_price=Sum("unit_price"),
    )

    result_filtered = Product.objects.filter(unit_price__gt=20).aggregate(
        count=Count("id"),
        min_price=Min("unit_price"),
        max_price=Max("unit_price"),
        avg_price=Avg("unit_price"),
        sum_price=Sum("unit_price"),
    )
    return render(
        request,
        "hello.html",
        {"name": "Check DBGtool", "result": result, "result_filtered": result_filtered},
    )

```

## Annotating objects

```html
<html>
  <body>
    {% if name %}
    <h1>{{ name }}</h1>
    {% else %}
    <h1>Hello World</h1>
    {% endif %}
    <div>{{queryset}}</div>
  </body>
</html>
```

```python
from django.shortcuts import render
from django.http import HttpResponse
from store.models import Product, OrderItem, Order, Customer
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q, F, Value, DecimalField, Func, ExpressionWrapper
from django.db.models import Count, Avg, Sum, Min, Max
from django.db.models.functions import Cast, Round


def say_hello(request):
    # Define the total_spent calculation as a separate variable for clarity
    total_spent_calculation = Sum(
        Round(
            ExpressionWrapper(
                F('order__orderitem__unit_price') * F('order__orderitem__quantity'),
                output_field=DecimalField(max_digits=5, decimal_places=2)
            ),
            2  # Round to 2 decimal places
        )
    )

    # Annotate the queryset with additional fields
    annotated_customers = Customer.objects.annotate(
        is_new=Value(True),  # Example of adding a constant value
        new_email=Value("New@test.com"),  # Another constant value
        new_id=F('id'),  # Replicating the ID, could be used for complex queries
        total_spent=total_spent_calculation  # Using the calculated total spent
    )

    # Render the response with the annotated queryset
    return render(
        request,
        "hello.html",
        {
            "name": "Check DBGtool",
            "queryset": annotated_customers
        }
    )

```

## Database functions

```python
def say_hello(request):
    # Annotate the queryset with additional fields
    annotated_customers = Customer.objects.annotate(
        # full_name = Func(F('first_name'), Value(' '), F('last_name'), function='CONCAT'),
        full_name=Concat(F('first_name'), Value(' '), F('last_name'))
    )

    # Render the response with the annotated queryset
    return render(
        request,
        "hello.html",
        {
            "name": "Check DBGtool",
            "queryset": annotated_customers
        }
    )

```

## Grouping data

```python
def say_hello(request):
    # Annotate the queryset with additional fields
    annotated_customers = Customer.objects.annotate(
        # orders_count = Count("order_set"),
        orders_count = Count("order"),
    ).order_by("-orders_count")[:5]

    # Render the response with the annotated queryset
    return render(
        request,
        "hello.html",
        {
            "name": "Check DBGtool",
            "queryset": annotated_customers
        }
    )
```

## Expression Wrappers

```python
def say_hello(request):
    # Annotate the queryset with additional fields
    discounted_price = ExpressionWrapper(
        # The unit price is the unit price minus the discount
        F("unit_price") * 0.8,
        # Tell Django what kind of field it is
        output_field=DecimalField(),
    )
    queryset = Product.objects.annotate(
        # Add a new field called "discount_price"
        discount_price=discounted_price
    )

    # Render the response with the annotated queryset
    return render(
        request, "hello.html", {"name": "Check DBGtool", "queryset": queryset}
    )

```

## Generic Relationships

```python
from django.shortcuts import render
from store.models import Product
from django.contrib.contenttypes.models import ContentType

from tags.models import TaggedItem


def say_hello(request):
    content_type = ContentType.objects.get_for_model(Product)
    tagged_items = TaggedItem.objects.select_related("tag").filter(content_type=content_type)

    # Render the response with the annotated queryset
    return render(
        request, "hello.html", {"name": "Check DBGtool", "queryset": list(tagged_items)}
    )
```

## Custom managers

```python
# views.py
from django.shortcuts import render
from store.models import Product
from django.contrib.contenttypes.models import ContentType

from tags.models import TaggedItem


def say_hello(request):
    tagged_items = TaggedItem.objects.get_tags_for(Product, 1)

    # Render the response with the annotated queryset
    return render(
        request, "hello.html", {"name": "Check DBGtool", "queryset": list(tagged_items)}
    )

```

```python
# models.py of tags
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


class TaggedItemManager(models.Manager): # New
    def get_tags_for(self, obj_type, obj_id):
        content_type = ContentType.objects.get_for_model(obj_type)
        return TaggedItem.objects.select_related("tag").filter(
            content_type=content_type, object_id=obj_id
        )


class Tag(models.Model):
    label = models.CharField(max_length=255)


class TaggedItem(models.Model):
    objects = TaggedItemManager() # New
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()
```

## Queryset cache

```python

from django.shortcuts import render
from store.models import Product


def say_hello(request):
    queryset = Product.objects.all()
    list(queryset) # This will trigger the query
    print(queryset[0]) # This will not trigger the query
    # Reverse of this will need two queries



    # Render the response with the annotated queryset
    return render(
        request, "hello.html", {"name": "Check DBGtool", "queryset": queryset}
    )
```

## Create objects

```python
from django.shortcuts import render
from store.models import Order, Product, OrderItem, Product, Customer,Collection


def say_hello(request):
    collection1 = Collection()
    collection1.title = "Video Games"
    collection1.featured_product = Product(pk=1)
    collection1.save()
    print(collection1.id)

    collection2 = Collection.objects.create(
        title="Music", featured_product=Product(pk=2)
    )
    print(collection2.id)
    # collection.featured_product_id = 1
    return render(
        request, "hello.html", {"name": "Check DBGtool"}
    )
```

## Update objects

```python
from django.shortcuts import render
from store.models import Order, Product, OrderItem, Product, Customer,Collection


def say_hello(request):
    # collection1 = Collection.objects.get(pk=11)
    # collection1.featured_product = Product(pk=3)
    # collection1.save()

    Collection.objects.filter(pk=11).update(featured_product=Product(pk=4))
    return render(
        request, "hello.html", {"name": "Check DBGtool"}
    )
```

## Delete objects

```python
def say_hello(request):
    collection = Collection(pk=11)
    collection.delete()

    Collection.objects.filter(id__gt=12).delete()
    return render(
        request, "hello.html", {"name": "Check DBGtool"}
    )

```

## Transaction control

```python
# @transaction.atomic
def say_hello(request):
    # ...
    with transaction.atomic():
        order = Order()
        order.customer_id = 1
        order.save()

        item = OrderItem()
        item.order = order
        item.product_id = 1
        item.quantity = 1    v
        item.unit_price = 100
        item.save()

    Collection.objects.filter(id__gt=12).delete()
    return render(request, "hello.html", {"name": "Check DBGtool"})
```

## Raw queries

```python
from django.shortcuts import render
from django.db import transaction,connection
from store.models import Order, Product, OrderItem, Product, Customer, Collection

# @transaction.atomic


def say*hello(request):
    # queryset = Product.objects.raw("SELECT * FROM store*product")
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM store_product")
        cursor.callproc('get_all_products',[1,2,3,4,5]),
        queryset = cursor.fetchall()
    return render(request, "hello.html", {"name": "Check DBGtool",'result':list(queryset)})
```
